## 1.将action的type统一放在一个actionTypes.js文件中

~~~
const change_input_value="change_input_value";
const add_todo_item="add_todo_item";
const delete_item="delete_item";
export {change_input_value,add_todo_item,delete_item};
~~~

> 当用使用的时候,导入这个文件,使用其中的变量,方便维护代码

## 2.创建actionCreators.js统一管理action