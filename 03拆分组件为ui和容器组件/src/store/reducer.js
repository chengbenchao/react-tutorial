import {change_input_value,add_todo_item,delete_item} from './actionTypes';
const defaultState={
    inputValue:'hello',
    list:[1,2,3]
};
/* state指store中的数据 */
/* reducer可以接收state,但不能修改state */
export default (state=defaultState,action)=>{
    /* ruducer拿到之前的数据,和action中传递过来的数据作比对 */
    if(action.type===change_input_value){
        const newState = {...state};
        newState.inputValue = action.value;
        return newState;
    }
    if(action.type===add_todo_item){
        const newState ={...state};
        newState.list.push(newState.inputValue)
        newState.inputValue=""
        return newState
    }
    if(action.type===delete_item){
        const newState = {...state};
        newState.list.splice(action.index,1);
        return newState;
    }
    return state;
    
}