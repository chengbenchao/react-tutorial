## 1.无状态组件

- UI组件只负责页面渲染的时候,可以使用无状态组件

~~~
import React from 'react';
import 'antd/dist/antd.css'
import { Input, Button, List } from 'antd';
const TodoListUI = (props) => {
    return (
        <div style={{ marginTop: "20px", marginLeft: "20px" }}>
            <Input
                value={props.inputValue}
                onChange={props.handleChange}
                style={{ width: 300, marginRight: "10px" }} />
            <Button type="primary" onClick={props.handleClick}>添加</Button>
            <List
                style={{ marginTop: "10px", width: "300px" }}
                bordered
                dataSource={props.list}
                renderItem={(item, index) => (<List.Item onClick={(index) => { props.handleDelete(index) }}>{item}</List.Item>)}
            />
        </div>
    )
}

export default TodoListUI;
~~~

## 2.Redux-thunk中间件中使用ajax

### 1.安装

~~~
npm i redux-thunk --save
~~~

- 在store/index.js文件中配置

~~~
import { createStore, applyMiddleware, compose } from 'redux';//++
import thunk from 'redux-thunk'; //++
import reducer from './reducer';
const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        }) : compose;
const enhancer = composeEnhancers(
    applyMiddleware(thunk)
);
const store = createStore(
    reducer,
    enhancer

);
export default store;
~~~

### 2.在action中发送http

~~~
import {change_input_value,add_todo_item,delete_item,http} from './actionTypes';
import axios from 'axios-jsonp-pro';
const getInputChangeAction =(value)=>{
    return {
        type:change_input_value,
        value
    }
}
const getAddItemAction =()=>{
    return {
        type:add_todo_item
    }
}
const getDeleteItemAction=(index)=>{
    return {
        type:delete_item,
        index
    }
}
const getHttpData=(data)=>{
    return {
        type:http,
        data
    }
}
const getTodoList=()=>{
    return (dispatch)=>{
        var url = "https://api.douban.com/v2/movie/top250"
        axios.jsonp(url).then(res=>{
            var subjects = res.subjects;
            var titles = []
            subjects.forEach(ele=>{
                var title = ele.title
                titles.push(title)
            })
            let action = getHttpData(titles);
            dispatch(action)
        })
    }
}
export {getInputChangeAction,getAddItemAction,getDeleteItemAction,getHttpData,getTodoList};
~~~

### 3.在组件中派发action

~~~
componentDidMount(){
        const action = getTodoList();
        store.dispatch(action);
}
~~~

### 4.在store/reducer.js中配置

~~~
import {change_input_value,add_todo_item,delete_item,http} from './actionTypes';
const defaultState={
    inputValue:'hello',
    list:[1,2,3]
};
/* state指store中的数据 */
/* reducer可以接收state,但不能修改state */
export default (state=defaultState,action)=>{
    /* ruducer拿到之前的数据,和action中传递过来的数据作比对 */
    if(action.type===change_input_value){
        const newState = {...state};
        newState.inputValue = action.value;
        return newState;
    }
    if(action.type===add_todo_item){
        const newState ={...state};
        newState.list.push(newState.inputValue)
        newState.inputValue=""
        return newState
    }
    if(action.type===delete_item){
        const newState = {...state};
        newState.list.splice(action.index,1);
        return newState;
    }
    //++
    if(action.type===http){
        const newState = {...state};
        newState.list=action.data;
        return newState;
    }
    return state;
    
}
~~~

