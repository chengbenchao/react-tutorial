import React from 'react';
import 'antd/dist/antd.css'
import { Input, Button, List } from 'antd';
const TodoListUI = (props) => {
    return (
        <div style={{ marginTop: "20px", marginLeft: "20px" }}>
            <Input
                value={props.inputValue}
                onChange={props.handleChange}
                style={{ width: 300, marginRight: "10px" }} />
            <Button type="primary" onClick={props.handleClick}>添加</Button>
            <List
                style={{ marginTop: "10px", width: "300px" }}
                bordered
                dataSource={props.list}
                renderItem={(item, index) => (<List.Item onClick={() => { props.handleDelete(index) }}>{item}</List.Item>)}
            />
        </div>
    )
}

export default TodoListUI;