import {change_input_value,add_todo_item,delete_item,http} from './actionTypes';
import axios from 'axios-jsonp-pro';
const getInputChangeAction =(value)=>{
    return {
        type:change_input_value,
        value
    }
}
const getAddItemAction =()=>{
    return {
        type:add_todo_item
    }
}
const getDeleteItemAction=(index)=>{
    return {
        type:delete_item,
        index
    }
}
const getHttpData=(data)=>{
    return {
        type:http,
        data
    }
}
const getTodoList=()=>{
    return (dispatch)=>{
        var url = "https://api.douban.com/v2/movie/top250"
        axios.jsonp(url).then(res=>{
            var subjects = res.subjects;
            var titles = []
            subjects.forEach(ele=>{
                var title = ele.title
                titles.push(title)
            })
            let action = getHttpData(titles);
            dispatch(action)
        })
    }
}
export {getInputChangeAction,getAddItemAction,getDeleteItemAction,getHttpData,getTodoList};