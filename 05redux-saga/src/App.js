import React, { Component } from 'react';
import store from './store/index';
import {getInputChangeAction,getAddItemAction,getDeleteItemAction,getHttpData} from './store/actionCreators';
import TodoListUI from './TodoListUI';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = store.getState();
        /* 订阅store的改变,只要store改变,handleStoreChange方法就会执行 */
        store.subscribe(this.handleStoreChange);
    }
    render() {
        return (
            <TodoListUI 
            list = {this.state.list}
            handleChange = {this.handleChange}
            handleClick={this.handleClick}
            handleDelete={this.handleDelete}
            inputValue = {this.state.inputValue}/>
        )
    }
    handleChange=(e)=>{
        let { value } = e.target;
        let action = getInputChangeAction(value);
        store.dispatch(action);
    }
    handleStoreChange=()=> {
        this.setState(store.getState())
    }
    handleClick=()=>{
        let action=getAddItemAction();
        store.dispatch(action);
    }
    handleDelete(index){
        let action=getDeleteItemAction(index);
        store.dispatch(action)
    }
    componentDidMount(){
        const action  = getHttpData();
        store.dispatch(action)
        
    } 
}
export default App;