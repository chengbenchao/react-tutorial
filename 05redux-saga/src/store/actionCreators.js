import {change_input_value,add_todo_item,delete_item,http} from './actionTypes';
const getInputChangeAction =(value)=>{
    return {
        type:change_input_value,
        value
    }
}
const getAddItemAction =()=>{
    return {
        type:add_todo_item
    }
}
const getDeleteItemAction=(index)=>{
    return {
        type:delete_item,
        index
    }
}
const getHttpData=(data)=>{
    return {
        type:http,
        data
    }
}
export {getInputChangeAction,getAddItemAction,getDeleteItemAction,getHttpData};