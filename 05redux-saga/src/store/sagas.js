import { takeEvery, put } from 'redux-saga/effects';
import { http } from './actionTypes';
import { getHttpData } from './actionCreators';
import axios from 'axios-jsonp-pro';
function* getListData() {
  try {
    var url = "https://api.douban.com/v2/movie/top250"
    const res = yield axios.jsonp(url);
    var subjects = res.subjects;
    var titles = []
    subjects.forEach(ele => {
      var title = ele.title
      titles.push(title)
    })
    const action = getHttpData(titles);
    yield put(action);
  }catch(e){
    console.log("地址无效")
  }
  
}
function* mySaga() {
  /* takeEvery捕捉action的类别 */
  /* 只要接受到getHttpData,就会执行fetchUser */
  /* 捕获action */
  yield takeEvery(http, getListData);
}

export default mySaga;