## react-redux

### 1.安装

~~~
yarn add react-redux
~~~

### 2.Provider组件

> Provider这个组件的作用: 它是react-redux的`核心组件`,它可以将store提供给它里面的每一个组件 

~~~js
import React from 'react';
import ReactDOM from 'react-dom';
import TodoList from './TodoList';
import store from './store';
import {Provider} from 'react-redux';
const App = (
    <Provider store={store}>
        <TodoList/>
    </Provider>
)
ReactDOM.render(App, document.getElementById('root'));
~~~

### 3.connect

> 但是只提供是不行的,如果组件想和store连接，还需要connect组件 

~~~js
//App.js组件
import {connect} from 'react-redux';
//怎么连接,connect方法接受三个参数
const mapStateToProps =(state)=>{
    return {
        inputValue:state.inputValue
    }
}
const mapDispatchProps=(dispatch)=>{
    return {
        changeInputValue(e){
            const action = {
                  type:'inputChange',
                  value:e.target.value
            }
            dispatch(action)
        }   
    }
}
export default connect(mapStateToProps,mapDispatchProps)(App)
//mapStateToProps表示将store的数据和组件的数据作映射
//将dispatch方法和store中的数据作关联
mapDispatchProps
// 可以让props的方法,派发action,操作store中的数据
~~~

