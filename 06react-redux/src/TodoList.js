import React, { Component } from 'react';
import {connect} from 'react-redux';
class TodoList extends Component {
  render() {
    const {inputValue,list,handleChange,handleAdd,handleDelete}=this.props;
    return (
      <div className="App">
         <input value={inputValue} onChange={handleChange} />
         <button onClick={handleAdd}>添加</button>
         <ul>
           {list.map((item,index)=>{
             return <li key={index} onClick={()=>{handleDelete(index)}}>{item}</li>
           })}
         </ul>
      </div>
    );
  }
}
const mapStateToProps =(state)=> {
  return {
    inputValue:state.inputValue,
    list:state.list
  }
}
// 可以让props的方法,派发action,操作store中的数据
const mapDispatchToProps=(dispatch)=>{
  return {
    handleChange(e){
      const action ={
        value:e.target.value,
        type:"valueChange"
      }
      dispatch(action)
    },
    handleAdd(){
      const action ={
        type:"add"
      }
      dispatch(action)
    },
    handleDelete(index){
      const action={
        type:"delete",
        value:index
      }
      dispatch(action)
    }
  }
  
}
export default connect(mapStateToProps,mapDispatchToProps)(TodoList)
