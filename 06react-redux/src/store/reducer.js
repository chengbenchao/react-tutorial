const defaultState = {
    inputValue:"hello world",
    list:[1]
}
export default (state=defaultState,action)=>{
    if(action.type==="valueChange"){
        let newState = {...state};
        newState.inputValue = action.value;
        return newState;
    }
    if(action.type==="add"){
       let newState ={...state};
       newState.list.push(newState.inputValue);
       newState.inputValue ="";
       return newState;
    }
    if(action.type==="delete"){
        let newState = {...state};
        newState.list.splice(action.value,1);
        return newState;
    }
    return state;
};