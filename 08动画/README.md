使用styled components，可将组件分为逻辑组件和展示组件**，逻辑组件只关注逻辑相关的部分，展示组件只关注样式**。通过解耦成两种组件，可以使代码变得更加清晰可维护。 

## 1.组件的class设置样式

~~~html
<Nav>
                    <NavItem className="left active">首页</NavItem>
                    <NavItem className="left">下载App</NavItem>
                    <NavItem className="right">Aa</NavItem>
                    <NavItem className="right">登录</NavItem>
</Nav>
~~~

~~~js
export const NavItem = styled.div`
  line-height: 58px;
  &.left {
    float: left;
  }
  &.right {
    float: right;
  }
  &.active{
    color:orangered;
  }
`;
~~~

## 2.设置属性

~~~
//语法
styled.a.attrs({
    href:'/'
})
~~~



~~~css
export const Logo = styled.a.attrs({
  href: "/"
})`
  position: absolute;
  left: 0;
  top: 0;
  display: block;
  width: 100px;
  height: 58px;
  background: url(${logoPic}) no-repeat center center;
  background-size: 100% 100%;
`;
~~~

## 3.小图标引入iconfont

~~~
//在入口文件index.js中引入iconfont
import './assets/iconfont/iconfont.css'
~~~

