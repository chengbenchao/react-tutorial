import React from 'react';
import ReactDOM from 'react-dom';
import App from './pages/App.js';
import './assets/iconfont/iconfont.css'
ReactDOM.render(< App />, document.getElementById('root'));
