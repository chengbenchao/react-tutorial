import React, { Component } from "react";
import {
  HeaderWrapper,
  Logo,
  Nav,
  NavItem,
  NavSearch,
  Addition,
  Button
} from "./style";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focused: false
    };
  }
  render() {
    return (
      <HeaderWrapper>
        <Logo />
        <Nav>
          <NavItem className="left active">首页</NavItem>
          <NavItem className="left">下载App</NavItem>
          <NavItem className="right">登录</NavItem>
          <NavItem className="right iconfont">&#xe636;</NavItem>
          <form>
            <NavSearch className={this.state.focused?'focused':''} />
            <i className="iconfont">&#xe653;</i>
          </form>
        </Nav>
        <Addition>
          <Button className="write">
            <i className="iconfont">&#xe603;</i>写文章
          </Button>
          <Button className="register">注册</Button>
        </Addition>
      </HeaderWrapper>
    );
  }
}
export default App;
