import styled from "styled-components";
import logoPic from "../../../assets/logo.png";
export const HeaderWrapper = styled.div`
  height: 58px;
  border-bottom: 1px solid #eee;
`;
export const Logo = styled.a.attrs({
  href: "/"
})`
  position: absolute;
  left: 0;
  top: 0;
  display: block;
  width: 100px;
  height: 58px;
  background: url(${logoPic}) no-repeat center center;
  background-size: 100% 100%;
`;
export const Nav = styled.div`
  position: relative;
  height: 100%;
  width: 960px;
  margin-left: auto;
  margin-right: auto;
  form {
    margin-top: 10px;
    width: 240px;
    height: 38px;
    position: absolute;
    left: 200px;
    i {
      position: absolute;
      font-size: 30px;
      right: 5px;
      top: 50%;
      transform: translateY(-50%);
    }
  }
`;
export const NavItem = styled.div`
  line-height: 58px;
  padding-left: 20px;
  padding-right: 20px;
  color: #333;
  &.left {
    float: left;
  }
  &.right {
    float: right;
    color: #969696;
  }
  &.active {
    color: orangered;
  }
`;
export const NavSearch = styled.input.attrs({
  placeholder: "搜索"
})`
  width: 220px;
  height: 38px;
  outline: none;
  border-radius: 19px;
  background: #eee;
  border: none;
  padding-left: 20px;
  &::placeholder {
    color: #999;
  }
`;
export const Addition = styled.div`
  height: 58px;
  position: absolute;
  right: 0;
  top: 0;
`;
export const Button = styled.div`
  float: right;
  margin-right: 20px;
  padding: 0 20px;
  margin-top: 9px;
  line-height: 38px;
  border-radius: 19px;
  border: 1px solid #ec6149;
  &.write {
    color: #fff;
    background: #ec6149;
  }
  &.register {
    color: #ec6149;
  }
`;
